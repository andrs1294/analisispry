/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import javax.swing.JFrame;

/**
 *
 * @author andrs1294
 */
public class graficaRecursos extends javax.swing.JFrame {

    /**
     * Creates new form graficaRecursos
     */
    public graficaRecursos(String val) {
        initComponents();
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        graficaRecursos1.setNombreGrafica(val);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        graficaRecursos1 = new modelo.vista.graficaRecursos();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        graficaRecursos1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                graficaRecursos1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout graficaRecursos1Layout = new javax.swing.GroupLayout(graficaRecursos1);
        graficaRecursos1.setLayout(graficaRecursos1Layout);
        graficaRecursos1Layout.setHorizontalGroup(
            graficaRecursos1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 388, Short.MAX_VALUE)
        );
        graficaRecursos1Layout.setVerticalGroup(
            graficaRecursos1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 288, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(graficaRecursos1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(graficaRecursos1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void graficaRecursos1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_graficaRecursos1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_graficaRecursos1MouseClicked

    
    public void agregarValor(long val,String operacion)
    {
         graficaRecursos1.agregarValor((int)val,operacion);
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private modelo.vista.graficaRecursos graficaRecursos1;
    // End of variables declaration//GEN-END:variables

}
