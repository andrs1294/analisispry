/**
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ $Id:
 * ArbolB.java,v 1.2 2008/09/22 20:29:21 alf-mora Exp $ Universidad de los Andes
 * (Bogot� - Colombia) Departamento de Ingenier�a de Sistemas y Computaci�n
 * Licenciado bajo el esquema Academic Free License version 2.1
 *
 * Proyecto Cupi2 (http://cupi2.uniandes.edu.co) Framework: Cupi2Collections
 * Autor: Jorge Villalobos - 25/02/2006 Autor: Pablo Barvo - 25/02/2006
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 */
package arbol.arbolB;

import java.io.Serializable;

import iterador.Iterador;
import iterador.IteradorException;
import iterador.IteradorSimple;
import arbol.ElementoExisteException;
import arbol.ElementoNoExisteException;
import arbol.IArbolOrdenado;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Implementaci�n de un �rbol B
 *
 * @param <T> Tipo de datos que contiene cada nodo del �rbol. Los nodos deben
 * implementar la interface Comparable
 */
public class ArbolB<T extends Comparable<? super T>> implements Serializable, IArbolOrdenado<T> {
    // -----------------------------------------------------------------
    // Constantes
    // -----------------------------------------------------------------

    /**
     * Constante para la serializaci�n
     */
    private static final long serialVersionUID = 1L;

    // -----------------------------------------------------------------
    // Atributos
    // -----------------------------------------------------------------
    /**
     * Ra�z del �rbol B
     */
    private NodoB<T> raiz;

    /**
     * Peso del �rbol B
     */
    private int peso;

    /**
     * Orden del �rbol B
     */
    private int orden;

    // -----------------------------------------------------------------
    // Constructores
    // -----------------------------------------------------------------
    /**
     * Construye un nuevo �rbol vac�o. <br>
     * <b>post: </b> Se construy� un �rbol vac�o, con ra�z null.
     */
    public ArbolB(int orden) {
        raiz = null;
        this.orden = orden;
    }

    // -----------------------------------------------------------------
    // M�todos
    // -----------------------------------------------------------------
    /**
     * Devuelve la ra�z del �rbol para ser navegada. <br>
     * <b>post: </b> Se retorn� la ra�z del �rbol.
     *
     * @return Ra�z del �rbol
     */
    public NodoB<T> darRaiz() {
        return raiz;
    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbol#buscar(java.lang.Object)
     */
    public T buscar(T modelo) {
        T resp;
        resp = (raiz != null) ? raiz.buscar(modelo) : null;
        return resp;

    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbol#darAltura()
     */
    public int darAltura() {
        return raiz == null ? 0 : raiz.darAltura();
    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbol#darPeso()
     */
    public int darPeso() {
        return peso;
    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbolOrdenado#insertar(java.lang.Comparable)
     */
    public void insertar(T elemento) throws ElementoExisteException {
        if (raiz == null) {
            // Caso 1: el �rbol es vac�o
            raiz = new NodoB<T>(elemento, orden);
        } else {
            // Caso 2: el �rbol no es vac�o
            raiz = raiz.insertar(elemento);
        }
        peso++;
    }

    /* (non-Javadoc)
     * @see uniandes.cupi2.collections.arbol.IArbolOrdenado#eliminar(java.lang.Comparable)
     */
    public void eliminar(T elemento) throws ElementoNoExisteException {
        if (raiz != null) {
            // Caso 1: el �rbol no es vac�o

            NodoB aux = raiz.eliminar(elemento);
//            if(raiz!=aux)
//            {
//                aux.setAltura(aux.getAltura()-1);
//            }
            raiz = aux;
            peso--;
        } else {
            // Caso 2: el �rbol es vac�o
            throw new ElementoNoExisteException("El elemento especificado no existe en el �rbol");
        }
    }

    private Vector<ArbolB<T>> crearConjuntoArboles(int p[], int[] s) {
        int c = 0;
        Vector<ArbolB<T>> ret = new Vector<>();
        if (p.length == 0) {
            ArbolB<T> arbol = new ArbolB(4);
            for (int ret1 : s) {
                try {
                    arbol.insertar((T) ((Integer) (ret1)));
                } catch (ElementoExisteException ex) {
                    Logger.getLogger(ArbolB.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            ret.add(arbol);
            return ret;
        }

        for (int i = 0; i < s.length; i++) {
            ArbolB<T> arbol = new ArbolB(4);
            while (i < s.length && s[i] != p[c]) {
                try {
                    arbol.insertar((T) ((Integer) (s[i])));
                    i++;
                } catch (ElementoExisteException ex) {
                    Logger.getLogger(ArbolB.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
            c++;
            ret.add(arbol);
            if (c == p.length) {
                ArbolB<T> ultimo = new ArbolB<>(4);
                for (; i < s.length; i++) {
                    try {
                        if (s[i] != p[c - 1]) {
                            ultimo.insertar((T) ((Integer) (s[i])));
                        }
                    } catch (ElementoExisteException ex) {
                        Logger.getLogger(ArbolB.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                ret.add(ultimo);
            }

        }

        return ret;
    }

    /**
     * Devuelve los elementos del �rbol en inorden. <br>
     * <b>post: </b> Se retorno el iterador para recorrer los elementos del
     * �rbol en inorden.
     *
     * @return Iterador para recorrer los elementos del �rbol en inorden.
     * Diferente de null.
     */
    public Iterador<T> inorden() {
        IteradorSimple<T> resultado = new IteradorSimple<T>(peso);
        if (raiz != null) {
            try {
                raiz.inorden(resultado);
            } catch (IteradorException e) {
                // Nunca deber�a lanzar esta excepci�n porque el tama�o del
                // iterador es el peso del �rbola
            }
        }
        return resultado;
    }

    /**
     * Devuelve los elementos del �rbol en inorden. <br>
     * <b>post: </b> Se retorno el iterador para recorrer los elementos del
     * �rbol en inorden.
     *
     * @return Iterador para recorrer los elementos del �rbol en inorden.
     * Diferente de null.
     */
    public LinkedList<T> inorden2() {
        LinkedList<T> resultado = new LinkedList<>();
        if (raiz != null) {

            raiz.inorden2(resultado);

        }
        return resultado;
    }

    public ArbolB<T> join(int x, ArbolB T1, ArbolB T2) throws ElementoExisteException {
        int resta = -1;
        if (T1.raiz != null && T2.raiz != null) {
            resta = T1.raiz.getAltura() - T2.raiz.getAltura();
        } else {
            if (T1.raiz == null && T2.raiz != null) {
                T2.insertar(x);
                return T2;
            } else if (T1.raiz != null && T2.raiz == null) {
                T1.insertar(x);
                return T1;
            }
        }

        if (resta > 0) {
            //NodoB z = T1.raiz.getNodoMasDerechaAlNivel(1,resta);

            T1.setRaiz(T1.raiz.insertarMaluco(x, resta, resta, T2.raiz));
            return T1;
        } else if (resta == 0) {
            T1.setRaiz(T1.raiz.insertarMaluco(x, 1, resta, T2.raiz));
            return T1;
        } else if (resta < 0) {

            T2.setRaiz(T2.raiz.insertarMaluco(x, (resta * -1), resta, T1.raiz));
            return T2;

        }

        return null;

    }

    public void setRaiz(NodoB<T> raiz) {
        this.raiz = raiz;
    }

}
