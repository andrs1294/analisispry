/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.vista;

import arbol.arbolB.ArbolB;
import arbol.arbolB.NodoB;
import java.awt.Color;
import java.awt.Graphics;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author Cristhian Cuero
 */
public class panelArbol extends javax.swing.JPanel {

    /**
     * Creates new form panelArbol
     */
    private ArbolB<Integer> raiz;
    LinkedList<HashMap<Integer, LinkedList<Integer>>> puntos;
    int xFinalRoot = 0;

    public panelArbol() {
        initComponents();
        puntos = new LinkedList<>();
    }

    public void recibirArbol(ArbolB<Integer> raiz) {
        
        this.raiz = raiz;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
       
        super.paintComponent(g);
        if (this.raiz != null && this.raiz.darRaiz() != null) {

            puntos.clear();
            int contProf = 1;
            int altura = this.raiz.darAltura();
            dibujarRecursivo2(g, this.raiz.darRaiz(), 0, 10, 0, contProf, altura - 1);
            dibujarlineas(g);
        }
    }
    /**
     * Name: dibujarRecursivo2
     * Purpose: dibuja el arbol
     * Inputs: NodoB n,nodo a dibujar
     *         int x,possicion en x en que se dibujara el nodo 
     *         int y, possicion en y en que se dibujara el nodo
     *         int xFinalPadre2, possicion en x en que se dibujara el nodo padre 
     *         int contProf, conteo de profundidad
     *         int altura altura del nodo
     * Outputs: arbol dibujado
     * Complexity: T(n)=(log(n)), en donde n es la cantidad de nodos 
     *  
    */   

    private int[] dibujarRecursivo2(Graphics g, NodoB n, int x, int y, int xFinalPadre2, int contProf, int altura) {

        if (n.esHoja()) {
            int xFinal = dubujarNodo2(g, x, y, n);
            return new int[]{xFinal, xFinalPadre2};
        } else {
            int c = n.darCantidadHijos();
            //int x2 = x - (240 * nivel) + (i2 * 120);
            int xFinalPadre = 0;
            boolean dibujadoPadre = false;
            int xFinalNodoAnterior = x;
            int c2 = 0;
            int nodosFinal[] = new int[4];
            int xFinalPadreDefinitivo = 0;
            for (int i = 0; i < c; i++) {
                int restul[] = dibujarRecursivo2(g, n.darHijo(i), xFinalNodoAnterior + 30, y + 80, xFinalPadre, contProf + 1, altura);
                xFinalNodoAnterior = restul[0];
                xFinalPadre = restul[1];
                if (contProf == altura) {
                    nodosFinal[c2++] = xFinalNodoAnterior;
                } else {
                    nodosFinal[c2++] = xFinalPadre;
                }

                //Dibujo el padre
                if (i == 1) {

                    xFinalPadre = dubujarNodo2(g, xFinalNodoAnterior, y, n);
                    dibujadoPadre = true;
                    xFinalPadreDefinitivo = xFinalPadre;
                   // if (n.getAltura() == altura + 1) {
                    // }
                }
            }
            if (!dibujadoPadre) {
                xFinalPadre = dubujarNodo2(g, x, y, n);
                xFinalPadreDefinitivo = xFinalPadre;
            }
            HashMap<Integer, LinkedList<Integer>> padre = new HashMap<>();
            LinkedList<Integer> hijos = new LinkedList<>();
            for (int i = 0; i < c; i++) {
                hijos.add(nodosFinal[i]);
            }
            hijos.add(y + 50);
            hijos.add(y + 80);

            padre.put(xFinalPadreDefinitivo, hijos);

               // padre.put(xFinalPadre, hijos);
            puntos.add(padre);
            //Dibujar lineas
//            for (int i = 0; i < c; i++) {
//                dibujarLinea(g, xFinalPadre-(30*(3-i)),finalNodos[i] , y + 50, y + 80);
//            }

            return new int[]{xFinalNodoAnterior, xFinalPadreDefinitivo};

        }

    }

        /**
     * Name: dubujarNodo2
     * Purpose: dibuja el nodo 
     * Inputs: NodoB n,nodo a dibujar
     *         int x,possicion en x en que se dibujara el nodo 
     *         int y, possicion en y en que se dibujara el nodo
     * Outputs: nodo dibujado
     * Complexity: O(n), en donde n es la cantidad de raices del nodo
     *  
    */   
    
    private int dubujarNodo2(Graphics g, int x, int y, NodoB n) {
        g.drawRect(x, y, 30, 50);
        g.drawRect(x + 30, y, 30, 50);
        g.drawRect(x + 60, y, 30, 50);
        int xFinal = x + 60 + 30;
        int aux = x + 5;
        Integer h = n.darAltura();
        g.setColor(Color.blue);
        g.drawString("" + h, x - 20, y);
        g.setColor(Color.black);
        for (int i = 0; i < n.darCantidadRaices(); i++) {
            String valor =  n.darRaiz(i).toString();
            if(valor.length()<3)
            {
               
            }else
            {
                if(valor.toLowerCase().contains("archivo"))
                {
                    valor=valor.substring(valor.length()-4);
                }else
                {
                    valor =""+ valor.substring(0,3);
                }
                
            }
            
            g.drawString(valor + "", aux, y + 30);
            aux += 30;
        }
        return xFinal;
    }

    private void dibujarLinea(Graphics g, int x1, int x2, int y1, int y2) {
        g.drawLine(x1, y1, x2, y2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        setBackground(new java.awt.Color(255, 255, 255));
        setPreferredSize(new java.awt.Dimension(1920, 1048));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 504, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 419, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
   
           /**
     * Name: dibujarlineas
     * Purpose: dibujar las lineas que unen los nodos del arbol, para brindar significado
     * Inputs: g
     * Outputs: linea dibujada
     * Complexity: O(n), en donde n es la cantidad de hijos de un  nodo
     *  
    */   
    
    
    private void dibujarlineas(Graphics g) {

        for (int i = 0; i < puntos.size(); i++) {
            HashMap<Integer, LinkedList<Integer>> aux = puntos.get(i);
            int xFinalPadre = aux.keySet().iterator().next();
            LinkedList<Integer> hijos = aux.values().iterator().next();
            for (int j = 0; j < hijos.size() - 2; j++) {
                dibujarLinea(g, xFinalPadre - (30 * (3 - j)), hijos.get(j) - 90, hijos.get(hijos.size() - 2), hijos.get(hijos.size() - 1));
            }

        }

    }
}
