/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.vista;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.LayoutManager;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import modelo.sistema.Objeto;

/**
 *
 * @author andrs1294
 */
public class PanelTop extends JPanel {

    private Object obj;
    LayoutManager layout;

    public PanelTop() {
        setBackground(Color.white);
        layout = this.getLayout();
    }

    @Override
    protected void paintComponent(Graphics g) {
        
        
        g.setColor(Color.white);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.black);
        mostrarObjeto(g);
    }

    public void recibirObjeto(Objeto o) {
        obj = o.getObjeto();

    }

    private void mostrarObjeto(Graphics g) {
        if (obj != null) {
            //this.removeAll();
            if (obj instanceof File) {
                mostrarArchivoFile((File) obj, g);
            } else if (obj instanceof String) {
                mostrarTexto((String) obj, g);
            }
        }

    }

    private void mostrarArchivoFile(File file, Graphics g) {
        String ruta = file.getAbsolutePath();
        String extension = obtenerExtension(ruta);
        if (extension.equalsIgnoreCase("txt")) {
            mostrarArchivoTexto(file, g);
        } else if (esUnaImagen(file)) {
            mostrarImagen(file, g);
        } else if (extension.equalsIgnoreCase("pdf")) {
            mostrarArchivoPDF(file, g);
        } else {
            mostrarOtrosObjetosConTextoEImagen(file, g);
        }
    }

    private String obtenerExtension(String ruta) {
        String extension = "";

        int i = ruta.lastIndexOf('.');
        if (i > 0) {
            extension = ruta.substring(i + 1);
        }
        return extension;
    }

    private void mostrarTexto(String msj, Graphics g) {
        g.drawString(msj, 10, 20);

    }

    /**
     * @param obj the obj to set
     */
    public void setObj(Object obj) {
        this.obj = obj;
    }

      /**
     * Name: mostrarArchivoTexto
     * Purpose: muestra la informacion contenida en un archivo de tecto
     * Inputs: file, archico a mostrar
     *         g
     * Outputs: informacion contenida en el archivo 
     * Complexity: O(n), en donde n es el numero de lineas del archivo
     *  
    */
    
    private void mostrarArchivoTexto(File file, Graphics g) {
        StringBuilder obj = new StringBuilder();
        obj.append("Archivo: " + file.getName() + "\n\n");
        Scanner entrada;
        try {
            entrada = new Scanner(file);
            while (entrada.hasNextLine()) {
                obj.append(entrada.nextLine() + "\n");
            }
            mostrarTexto(obj.toString(), g);
        } catch (FileNotFoundException ex) {
            mostrarTexto("Error, el archivo no existe en la ubicacion " + file.getAbsolutePath(), g);
        }

    }

    private boolean esUnaImagen(File file) {
        String extension = obtenerExtension(file.getAbsolutePath());
        boolean retorno = extension.equalsIgnoreCase("jpg");
        retorno |= extension.equalsIgnoreCase("png");
        retorno |= extension.equalsIgnoreCase("jpeg");
        retorno |= extension.equalsIgnoreCase("gif");

        return retorno;
    }

    private void mostrarImagen(File file, Graphics g) {
        ImageIcon img = new ImageIcon(file.getAbsolutePath());
        g.drawImage(img.getImage(), 20, 30, 200, 200, this);
    }

    private void mostrarOtrosObjetosConTextoEImagen(File file, Graphics g) {
        File imgArchivoDesconocido = new File("src/imagenes/other.gif");
        mostrarImagen(imgArchivoDesconocido, g);
        g.drawString("Archivo: " + file.getName(), 30, 220);
    }

    private void mostrarArchivoPDF(File file, Graphics g) {
        File imgArchivoDesconocido = new File("src/imagenes/pdf.png");
        mostrarImagen(imgArchivoDesconocido, g);
        String as = "Archivo: " + file.getName();
        g.drawString(as, 30, 250);
    }

}
