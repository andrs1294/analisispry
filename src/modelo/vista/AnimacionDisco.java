/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.vista;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import modelo.sistema.hiloAnimacion;

/**
 *
 * @author Cristhian Cuero
 */
public class AnimacionDisco extends javax.swing.JPanel {

    /**
     * Creates new form AnimacionDisco
     */
    private final ImageIcon imgSystem;
    private final ImageIcon imgHdd;
    private final ImageIcon imgRam;
    private final ImageIcon imgPagina;
    private final ImageIcon imgObjeto;
    private final ImageIcon imgReferecia;
    private final ImageIcon impPregunta;
    private final ImageIcon imgEliminar;
    private Rectangle computador;
    private Rectangle hdd;
    private Rectangle ram;
    private Rectangle pagina;
    private Rectangle referencia;
    private boolean animando;
    private int graficarObjeto;
    private boolean animar;
    private int cualAnimacion;
    private String mensaje="";

    public AnimacionDisco() {
        initComponents();
        imgSystem = new ImageIcon("src/imagenes/system.png");
        imgHdd = new ImageIcon("src/imagenes/hdd.png");
        imgRam = new ImageIcon("src/imagenes/ram.png");
        imgPagina = new ImageIcon("src/imagenes/pagina.png");
        imgObjeto = new ImageIcon("src/imagenes/objeto.png");
        imgReferecia = new ImageIcon("src/imagenes/referencia.png");
        impPregunta = new ImageIcon("src/imagenes/pregunta.png");
        imgEliminar = new ImageIcon("src/imagenes/eliminar.png");
        graficarObjeto = 1;
//        establecerRectangulos();

    }

    public void establecerRectangulos() {
        computador = new Rectangle(10, 10, 128, 128);
        hdd = new Rectangle(10, this.getHeight() - 128, 128, 128);
        ram = new Rectangle(this.getWidth() - 128, 10, 128, 128);
        pagina = new Rectangle(this.getWidth() - 90, this.getHeight() - 100, 128, 128);
        setReferencia(new Rectangle(59, 49, 64, 64));
    }

    @Override
    protected void paintComponent(Graphics g) {
        dibujarGrafo(g);
        mostrarMensajeInformativo(g);
        
        if (animar) {
            
            if (graficarObjeto == 1) {
                g.drawImage(imgObjeto.getImage(), referencia.x, referencia.y, referencia.width, referencia.height, this);
            } else if (graficarObjeto == 2) {
                g.drawImage(imgReferecia.getImage(), referencia.x, referencia.y, referencia.width, referencia.height, this);
            } else if (graficarObjeto == 3) {
                g.drawImage(imgPagina.getImage(), referencia.x, referencia.y, referencia.width, referencia.height, this);
            } else if (graficarObjeto == 4) {
                g.drawImage(impPregunta.getImage(), referencia.x, referencia.y, referencia.width, referencia.height, this);
            } else if (graficarObjeto == 5) {
                g.drawImage(imgEliminar.getImage(), referencia.x, referencia.y, referencia.width, referencia.height, this);
            }
            
            
            
            if (!animando) {

                animando = true;
                new hiloAnimacion(cualAnimacion, this);
                
            }
        }

    }

    public void animacionTerminada() {
        animar=false;
        animando = false;
        mensaje="";
        repaint();
        cualAnimacion=-1;
    }

    private void dibujarGrafo(Graphics g) {
        g.setColor(Color.white);
        g.fillRect(0, 0, this.getWidth(), this.getHeight());
        g.setColor(Color.BLUE);
        g.drawLine((138 / 2) + 20, 138 / 2, (138 / 2) + 20, this.getHeight() - 120);
        g.drawLine((138 / 2) + 20, 138 / 2, this.getWidth() - 120, 138 / 2);
        g.drawLine((138 / 2) + 20, 138 / 2, this.getWidth() - 120, 138 / 2);
        g.drawLine(this.getWidth() - 60, 138 / 2, this.getWidth() - 60, this.getHeight() - 88);
        g.drawImage(imgSystem.getImage(), 10, 10, 128, 128, this);
        g.drawImage(imgHdd.getImage(), 10, this.getHeight() - 128, 128, 128, this);
        g.drawImage(imgRam.getImage(), this.getWidth() - 128, 10, 128, 128, this);
        g.drawImage(imgPagina.getImage(), this.getWidth() - 90, this.getHeight() - 100, this);
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

     /**
     * Name: animacion
     * Purpose: dada la operacion y la cantidad de accesos a disco decide que animacion se ha de realizar
     * Inputs: a, cantidad de accesos a disco
     *         operacion, push o pop
     *         im, implementacion actual del sistema (0 ó 1) 
     * Outputs: realiza la correspondiente animacion
     * Complexity: O(1)
     *  
    */
    public void animacion(int a, String operacion,int im) {

        if(operacion.equals("push"))
        {
            if(im==0)
            {
                if(a==2)
                {
                   cualAnimacion=1;
                }else if(a==3)
                {
                    cualAnimacion=2;
                }
            }else if(im==1)
            {
                 if(a==1)
                {
                   cualAnimacion=5;
                }else if(a==2)
                {
                    cualAnimacion=6;
                }
                else if(a==3)
                {
                     cualAnimacion=9;
                }
                else if(a==4)
                {
                     cualAnimacion=10;
                }
            }
        }else if(operacion.equals("pop"))
        {
             if(im==0)
            {
                if(a==3)
                {
                   cualAnimacion=3;
                }else if(a==4)
                {
                    cualAnimacion=4;
                }
            }else if(im==1)
            {
                if(a==2)
                {
                   cualAnimacion=7;
                }else if(a==4)
                {
                    cualAnimacion=8;
                }
                else if(a==5)
                {
                    cualAnimacion=11;
                }
            }
        }
        
        animar=true;
        repaint();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    // End of variables declaration//GEN-END:variables
    /**
     * @return the computador
     */
    public Rectangle getComputador() {
        return computador;
    }

    /**
     * @return the hdd
     */
    public Rectangle getHdd() {
        return hdd;
    }

    /**
     * @return the ram
     */
    public Rectangle getRam() {
        return ram;
    }

    /**
     * @return the pagina
     */
    public Rectangle getPagina() {
        return pagina;
    }

    /**
     * @return the referencia
     */
    public Rectangle getReferencia() {
        return referencia;
    }

    public void animarReroObj(int b) {
        graficarObjeto = b;
    }

    /**
     * @param referencia the referencia to set
     */
    public void setReferencia(Rectangle referencia) {
        this.referencia = referencia;
    }

    /**
     * Name: mostrarMensajeInformativo
     * Purpose: muestra un mensaje que brinda significado a la animacion que se esta ejectando 
     * Inputs: Graphics g
     * Outputs: los mensajes informativos
     * Complexity: O(n), en donde n es el tamaño del vector aux
     *  
    */
    
    private void mostrarMensajeInformativo(Graphics g) {
        if(mensaje.length()>30)
        {
            String tex1="";
            String tex2="";
            String aux[] = mensaje.split(" ");
            for (int i = 0; i < aux.length; i++) {
                if(i<aux.length/2)
                {
                    tex1+=aux[i]+" ";
                }else
                {
                    tex2+=aux[i]+" ";
                }
            }
            g.drawString(tex1, this.getWidth()/2-90, this.getHeight()/2);
            g.drawString(tex2, this.getWidth()/2-90, this.getHeight()/2 + 20);
        }else
        {
            g.drawString(mensaje, this.getWidth()/2-90, this.getHeight()/2);
        }
        
    }

}
