/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sistema;

import Excepciones.discoLleno;
import Excepciones.ramLlenaExcepcion;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andrs1294
 */
public class Ram {

    /**
     * @param aCantidadPalabrasPaginas the cantidadPalabrasPaginas to set
     */
    public static void setCantidadPalabrasPaginas(int aCantidadPalabrasPaginas) {
        cantidadPalabrasPaginas = aCantidadPalabrasPaginas;

    }

    int[] ram;
    int[] paginasPointer;
    static int cantidadPalabrasPaginas;
    private Sistema system;
    private short implementacion;   // 0 para la implementacio ineficiente, 1 para la implementacion donde la pagina ya se encuentra en la ram.
    boolean yaSeCargo;

    public Ram(int cantidad, Sistema sys, int implementacion) {

        ram = new int[cantidad / 2]; //Lo divido en 2 puesto que la cantidad me llega en Bytes y voy a utilizar una palabra de 16 bits.
        system = sys;
        this.implementacion = (short) implementacion;
        if (implementacion == 1) {
            ram[0] = Integer.MAX_VALUE;
        }
        cantidadPalabrasPaginas = 0;

    }

    /**
     * name:agregarEntrada
     * purpose:Agrega la direccion de ese objeto en la pagina que ya se encuentra al
     * final de la Ram, me retorna la pagina que se ha de guardar para la
     * implementacion 1 si la pagina actual ya esta completamente llena para
     * poder ingresar la referencia del nuevo valor, si es la implementacion 0
     * me devuelve un null
     * inputs:Entero que me indica la referencia del objeto a agregar, un entero que me indica la 
     * posicion dentro de la pagina a insertarlo y la pagina dentro de la cual se debe ingresar la pagina.
     * outputs:Un objeto que me devuelve la pagina que esta actualemten en memoria
     * y donde fue almacenado el archivo
     * complexity: O(1)
     * @param hexObj
     * @param posElemento meIndica en que posicion de la pagina debo agregar esa
     * direccion
     */
    public Objeto agregarEntrada(int hexObj, int posElemento, int pagina) {
        if (getImplementacion() == 0) {
            ram[ram.length - 1 - posElemento] = hexObj;
        } else {
            Objeto obj = null;
            if (posElemento == 0) {

                //Este if verifica que si ya existe algun valor en la posicion cero es porque debo crear esa pagina que ya esta llena para guardarla en el disco
                if (ram[ram.length - 1] != 0) {
                    Pagina pg = this.obtenerPaginaResidente();
                    obj = new Objeto(pg);
                    ram[pagina - 1] = obj.hashCode();
                    ponerPaginaEnRam(new Pagina()); //pongo el final en ceros
                }
            }

            ram[ram.length - 1 - posElemento] = hexObj;
            //Si el sistema esta en manual, no debo decir que la proxima pagina ya esta en memoria
            if (Sistema.automatico) {
                if (paginaResidenteLlena()) {
                    ram[pagina + 1] = Integer.MAX_VALUE;
                }
            }

            return obj;
        }
        return null;
    }

    /**
     * name:colocarPagina
     * purpose:Este metodo coloca la pagina correspondiente al final de la memoria ram,
     * si es la implementacion 1 me devuelve un null
     * inputs:Entero que me indica la posición dentro de la RAM de la pagina la cual quiero establecer
     * en memoria RAM
     * outputs:Un objeto que representa la pagina donde se ha de ingresar el objeto o 
     * null si la implementacion es la 1
     * complexity: O(1)
     * @param pagina Numero de la pagina a traer
     */
    Objeto colocarPagina(int pagina) {
        if (getImplementacion() == 0) {
            int paginaHex = getPagina(pagina);
            Objeto obj;
            if (paginaHex == 0) //Si es 0 me indica que la pagina no existe en disco, primero debo crearla
            {
                Pagina pg = new Pagina();
                obj = new Objeto(pg);
                ram[pagina] = obj.hashCode();
                ponerPaginaEnRam(pg);
                return obj;

            }

            Objeto pag = ponerPaginaDelDiscoEnRam(paginaHex);
            //Cuando la implementacion es manual, ya he cargado esta pagina anteriormente y debo eliminar un acceso a disco =)
            if (yaSeCargo) {
                if (!Sistema.automatico) {
                    system.getDisco().eliminarAccesoDisco();
                }
                yaSeCargo = false;
            } else {
                if (!Sistema.automatico) {
                    yaSeCargo = true;
                }
            }

            return pag;
        } else if (getImplementacion() == 1) {
            if (ram[pagina] != Integer.MAX_VALUE) {
                Objeto pag = ponerPaginaDelDiscoEnRam(getPagina(pagina));
                system.eliminarPaginaDelDisco(getPagina(pagina));
                ram[pagina] = Integer.MAX_VALUE;//con esto le indico que ya esta en memoria
                //Estalezco el top de esa pagina en memoria ram
                if (!Sistema.automatico) {
                    ram[ram.length - 1 - cantidadPalabrasPaginas] = obtenerLlenoPagina(pag);
                }

            }
        }
        return null;
    }

    /**
     * name:ponerPaginaDelDiscoEnRam
     * purpose: Dada una direccion hexadecimal de una pagina, la busca en el disco, la
     * coloca en la ram y retorna la pagina
     * inputs:Entero que me indica la referencia de la pagina que quiero buscar en el disco
     * outputs:Un objeto que representa la pagina con esa referencia
     * complexity: O(1)
     * @param paginaHex La referecia hexadecial de la pagina a buscar en disco
     * @return La pagina buscada en el disco
     */
    private Objeto ponerPaginaDelDiscoEnRam(int paginaHex) {
        Objeto pag = system.obtenerObjeto(paginaHex);
        Pagina paginaEnDisco = (Pagina) pag.getObjeto();
        ponerPaginaEnRam(paginaEnDisco);
        return pag;
    }

    /**
     * name:ponerPaginaEnRam
     * purpose: Coloca la pagina dada al final de la ram
     * inputs:Un objeto pagina 
     * outputs:
     * complexity: O(n) donde n es la cantidad de palabras por pagina - m
     * @param paginaAPoner Pagina a poner
     */
    private void ponerPaginaEnRam(Pagina paginaAPoner) {
        int[] pg = paginaAPoner.getPagina();
        int c = 0;
        for (int i = ram.length - getCantidadPalabrasPaginas(); i < ram.length; i++) {
            ram[i] = pg[c++];
        }
    }

    /**
     * name:obtenerPaginaResidente
     * purpose: Crea la pagina que se encuentra actualmente en memoria RAM
     * inputs: 
     * outputs:Retorna El obtejo Pagina de la pagina que reside actualmente en memoria 
     * complexity: O(n) donde n es la cantidad de palabras por pagina - m
     * @return
     */
    public Pagina obtenerPaginaResidente() {
        int pagina[] = new int[getCantidadPalabrasPaginas()];
        int c = 0;
        for (int i = ram.length - getCantidadPalabrasPaginas(); i < ram.length; i++) {
            pagina[c++] = ram[i];
        }

        Pagina pg = new Pagina(pagina);
        return pg;
    }

    /**
     * name:porcentajeOcupado
     * purpose: indica el porcentaje del nivel de espacio libre en la RAM
     * inputs: Un entero que representa la operacion que se realizó 
     * 0 para indicar push, 1 para indicar pop, se requiere
     * este valor para no alterar el porcentaje
     * outputs:un numero entre 1 - 100 del porcentaje de la ram ocupada
     * complexity: O(n) donde n es la cantidad de paginas actualmente en memoria RAM
     * @param operacion 0 para indicar push, 1 para indicar pop, se requiere
     * este valor para no alterar el porcentaje
     * @return un numero entre 1 - 100 del porcentaje de la ram ocupada
     */
    float porcentajeOcupado(int operacion) {
        int cantidadLleno = 0;
        if (getImplementacion() == 0) {
            for (int i = 0; i < ram.length - Ram.cantidadPalabrasPaginas - 1; i++) {
                if (ram[i] != 0) {
                    cantidadLleno++;
                }
            }

            return (cantidadLleno * 100f) / ram.length;

        } else {

            for (int i = 0; i < ram.length; i++) {
                if (ram[i] != 0) {
                    cantidadLleno++;
                }
            }

            return (cantidadLleno * 100f) / ram.length;
        }
    }

    /**
     * name:eliminarTop
     * purpose: Elimina el top de una pagina y me retorna el objeto
     * inputs: Entero que indica la posicion donde se ha eliminar el objeto, Pagina
     * donde se encuentra el Top
     * outputs:Retorna El obtejo que fue eliminado de la pila
     * complexity: O(1) 
     */
    Objeto eliminarTop(int posicion, Objeto pagina) {
        int hexObjeto = ram[ram.length - 1 - posicion];
        if (hexObjeto != 0) {
            Objeto retorno = system.obtenerObjeto(hexObjeto);
            Pagina pg = (Pagina) (pagina.getObjeto());
            int[] paginaRam = pg.getPagina();
            paginaRam[paginaRam.length - posicion - 1] = 0;
            pg.setPagina(paginaRam);
            return retorno;
        }
        return null;

    }

    /**
     * name:size
     * purpose:Indica el numero de paginas existentes en Disco y ram
     * inputs: 
     * outputs:Retorna el numero de paginas existentes en Disco
     * complexity: O(n) donde n es la cantidad de paginas en disco  
     * @return el numero de paginas existentes en Disco
     */
    public int size() {
        int res = 0;
        for (int i = 0; i < this.ram.length; i++) {
            if (this.ram[i] != 0 && this.ram[i] != Integer.MAX_VALUE) {
                res++;
            } else {
                break;
            }
        }
        if (system.getImplementacion() == 1 && !Sistema.automatico) {
            for (int i = 0; i < this.ram.length; i++) {
                if (this.ram[i] != 0) {
                    res++;
                } else {
                    break;
                }
            }
        }
        return res;
    }

    /**
     * @return the cantidadPalabrasPaginas
     */
    public static int getCantidadPalabrasPaginas() {
        return cantidadPalabrasPaginas;
    }

    void borrarPagina(int pagina) {
        ram[pagina] = 0;
    }

    /**
     * @return the implementacion
     */
    public short getImplementacion() {
        return implementacion;
    }

    Objeto eliminarTopImp2(int posicion, int pagina) {
        int hexObjeto = ram[ram.length - 1 - posicion];
        if (hexObjeto != 0) {
            Objeto retorno = system.obtenerObjeto(hexObjeto);
            ram[ram.length - posicion - 1] = 0;
            if (paginaResidenteVacia()) {
                if (pagina >= 1) {
                    // system.eliminarPaginaDelDisco(ram[pagina-1]);
                    if (Sistema.automatico) {
                        ram[pagina] = 0;
                    }
                }

            }
            return retorno;
        }
        return null;
    }

    /**
     * name:paginaResidenteVacia
     * purpose: Indica si la pagina residente en RAM esta vacia
     * inputs: 
     * outputs: Verdadero si la pagina esta vacia
     * complexity: O(n) donde n es la cantidad de palabras por pagina
     */
    private boolean paginaResidenteVacia() {
        for (int i = ram.length - cantidadPalabrasPaginas; i < ram.length; i++) {
            if (ram[i] != 0) {
                return false;
            }
        }
        return true;
    }

     /**
     * name:paginaResidenteLlena
     * purpose: Indica si la pagina residente en RAM esta llena
     * inputs: 
     * outputs: Verdadero si la pagina esta llena, falso de lo contrario
     * complexity: O(n) donde n es la cantidad de palabras por pagina
     */
    private boolean paginaResidenteLlena() {
        for (int i = ram.length - cantidadPalabrasPaginas; i < ram.length; i++) {
            if (ram[i] == 0) {
                return false;
            }
        }
        return true;

    }

    int getTop() {
        return ram[ram.length - 1 - cantidadPalabrasPaginas];
    }

    void incrementarTop() {
        ram[ram.length - 1 - cantidadPalabrasPaginas]++;
    }

    void decremetarTop(int pag) {
        if (Sistema.automatico || implementacion == 1 || pag == -1) {
            ram[ram.length - 1 - cantidadPalabrasPaginas]--;
        } else {
            ram[pag]--;
        }
    }

    int getRangoPaginas() {
        int fin = -1;
        for (int s : ram) {
            if (s == 0) {
                break;
            }
            fin++;
        }

        return fin;
    }

    /**
     * name:crearPaginasManual
     * purpose: Si se ha escogido la implementacion manual, este metodo crea las paginas iniciales del sistema
     * inputs: Cantidad de paginas a crear
     * outputs:
     * complexity: O(n) donde n es la cantidad de paginas a crear
     */
    void crearPaginasManual(int cantidadPag) throws ramLlenaExcepcion {
        paginasPointer = new int[cantidadPag];
        int maximo=ram.length-cantidadPalabrasPaginas-1;
        if(cantidadPag>maximo)
        {
            throw new ramLlenaExcepcion("Ah excedido la candidad de paginas a crear en memoria RAM, solo puede almacenar un maximo de: " + maximo);
        }
        for (int i = 0; i < cantidadPag; i++) {
            ram[i] = crearPaginaYGuardarEnDisco() - 1;
            paginasPointer[i] = ram[i] + 1;//le resto uno para que cuando vaya a ingresar un elemento y la pagina este vacia, al hacer la resta me diga que debo ingresarlo en la posicion 0
        }
    }

    /**
     * name:crearPaginaYGuardarEnDisco
     * purpose:Crea una pagina vacia y la guarda en Disco
     * inputs:
     * outputs:
     * complexity: O(1)
     */
    private int crearPaginaYGuardarEnDisco() {
        Pagina pg = new Pagina();
        Objeto obj = new Objeto(pg);
        try {
            return system.guardarObjetoEnDisco(obj);
        } catch (discoLleno ex) {
            Logger.getLogger(Ram.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }

    int getPagina(int pag) {
        if (Sistema.automatico) {
            return ram[pag];
        }
        return paginasPointer[pag];
    }

    /**
     * name:incrementarPointer
     * purpose:Incrementa el stackpointer de la pila o de una pagina, segun si es manual o automatizado
     * inputs: Entero que me indica la posicion dentro de la RAM para buscar el Top 
     * outputs:El entero que me indica el Top de la pila o pagina
     * complexity: O(1)
     */
    int getPosElemento(int pag) {
        if (ram[pag] == Integer.MAX_VALUE) {
            return getTop();
        }
        if (!Sistema.automatico) {
            guardarEnDiscoPaginaActualEnRam(pag);
            colocarPagina(pag);
        }
        if(implementacion==1 && !Sistema.automatico)
        {
            return getTop();
        }
        return ram[pag] - paginasPointer[pag];
    }

    /**
     * name:incrementarPointer
     * purpose:Incrementa el stackpointer de la pila o de una pagina, segun si es manual o automatizado
     * inputs: Entero que me indica la posicion dentro de la RAM para incrementar el puntero
     * outputs:
     * complexity: O(1)
     */
    void incrementarPointer(int pag) {
        //Esta verificacion se realiza para no alterar el valor que me indica que una pagina se encuentra actualmente en memoria
        if (ram[pag] != Integer.MAX_VALUE) {
            if (implementacion == 1) {
                if (!Sistema.automatico) { //Esta verificacion es redundante, quien llama ha este metodo ya lo ha verificado
                    guardarEnDiscoPaginaActualEnRam(pag);
                }
                colocarPagina(pag);
                ram[ram.length - 1 - cantidadPalabrasPaginas]++;
                return;
            }
            ram[pag]++;
        } else {

            //incremento la posicion dada para almacenar el top de la pagina que esta actualmente en memoria
            ram[ram.length - 1 - cantidadPalabrasPaginas]++;
        }
    }

    /**
     * name:obtenerLlenoPagina
     * purpose:Indica que tan llena se encuentra una pagina
     * inputs: Objeto pagina del cual se requiere saber que tan lleno se encuentra
     * outputs:Retorna el entero que indica que tan llena se encuentra una pagina
     * complexity: O(n) donde n es la cantidad de palabras para la pagina
     */
    private int obtenerLlenoPagina(Objeto pag) {
        Pagina pg = (Pagina) pag.getObjeto();
        int pagi[]=pg.getPagina();
        int can = -1;
        for (int i = pagi.length-1; i>=0; i--) {
            if (pagi[i] == 0) {
                break;
            }
            can++;
        }
        
        return can;
    }

    /**
     * name:posicionPaginaActualMemo
     * purpose:Indica la posicion de la pagina actual en memoria dentro del vector RAM
     * inputs: 
     * outputs:Retorna la posicion de la pagina actual en memoria
     * complexity: O(n) donde n es la cantidad de paginas en Disco
     * @return posicion de la pagina actual en memoria
     */
    private int posicionPaginaActualMemo() {
        for (int i = 0; i < ram.length; i++) {
            if (ram[i] == Integer.MAX_VALUE) {
                return i;
            }
        }
        return -1;
    }

    /**
     * name:guardarEnDiscoPaginaActualEnRam
     * purpose:Permite guardar la pagina actual en disco, esto porque estoy utilizando
     * la implementacion 1 en manual y me han pedido otra pagina
     * inputs: La pagina que me han pedido almacenar en disco
     * outputs:
     * complexity: O(1)
     */
    private void guardarEnDiscoPaginaActualEnRam(int pagina) {
        try {
            int pagActual = posicionPaginaActualMemo();
            if (pagina != pagActual && pagActual != -1) { //La ultima verificacion donde se tiene el -1 me indica que ninuga pagina esta actualemtne en memoria
                Pagina residente = obtenerPaginaResidente();
                Objeto pa = new Objeto(residente);
                int hash = system.guardarObjetoEnDisco(pa);
                ram[pagActual] = hash;
                paginasPointer[pagActual] = hash;
            }

        } catch (discoLleno ex) {
            Logger.getLogger(Ram.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
