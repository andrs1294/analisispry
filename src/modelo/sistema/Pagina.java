/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modelo.sistema;

/**
 *
 * @author Cristhian Cuero
 */
public class Pagina {

    private int pagina[];

    public Pagina() {
        pagina= new int[Ram.getCantidadPalabrasPaginas()];
    }
    
    
    Pagina(int[] pagina) {
       this.pagina=pagina;
    }

    public int[] getPagina() {
        return pagina;
    }

    public void setPagina(int[] pagina) {
        this.pagina = pagina;
    }
    
    
    /**
     * 
     * @return catidad en bytes que pesa la pagina
     */
    public int lengthEnBytes()
    {
        return pagina.length;
    }
    
    /**
     * name:isVacia
     * purpose:Indica si la pagina ya se encuentra vacia
     * inputs:
     * outputs:Un boolean que es verdadero cuando la pagina esta totalmente vacia, falso de lo contrario
     * complexity: O(n) donde n es la cantidad de palabras - m
     * @return verdadero si no tiene referencias, falso si tiene referencias a objetos
     */
    public boolean isVacia()
    {
        boolean vacia=true;
        for (int i = 0; i < pagina.length; i++) {
            if(pagina[i]!=0)
            {
                vacia = false;
                break;
            }
        }
        return vacia;
    }
}
