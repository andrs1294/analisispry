/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sistema;

import java.awt.Point;
import java.awt.Rectangle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;
import modelo.vista.AnimacionDisco;

/**
 *
 * @author Cristhian Cuero
 */
public class hiloAnimacion extends Thread {

    private int animacion;
    private Rectangle computador;
    private Rectangle hdd;
    private Rectangle ram;
    private Rectangle pagina;
    private Rectangle objeto;
    private Rectangle referencia;
    private Rectangle paginaPeq;
    private AnimacionDisco panel;
    private int animacionNum;

    public hiloAnimacion(int animacion, AnimacionDisco panel) {
        this.animacion = animacion;
        animacionNum = animacion;
        this.panel = panel;
        this.start();
    }

    @Override
    public void run() {

        computador = panel.getComputador();
        hdd = panel.getHdd();
        pagina = panel.getPagina();
        ram = panel.getRam();

        if (animacionNum == 1) {
            animarPushConCrearPaginaImp0();
        } else if (animacionNum == 2) {
            animarPushConBuscarPaginaImp0();
        } else if (animacionNum == 3) {
            animarPopSinEliminarPaginaImp0();
        } else if (animacionNum == 4) {
            animarPopConEliminarPaginaImp0();
        } else if (animacionNum == 5) {
            animarPushSinCrearPaginaImp1();
        } else if (animacionNum == 6) {
            animarPushConCrearPaginaImp1();
        } else if (animacionNum == 7) {
            animarPopSinBuscarPaginaImp1();
        } else if (animacionNum == 8) {
            animarPopConBuscarPaginaImp1();
        } else if (animacionNum == 9) {
            animarPushConBuscarPaginaImp1Manual();
        } else if (animacionNum == 10) {
            animarPushConGuardarPGActaulYBuscarOtraPaginaImp1Manual();
        } else if (animacionNum == 11) {
            animarPopConGuardarPGActualYBuscarOtraPaginaImp1Manual();
        }

        panel.animacionTerminada();

    }

    private void animar(Rectangle origen, Rectangle destino, int animar, boolean horizontal, String mensaje) {
        panel.animarReroObj(animar);
        this.panel.setMensaje(mensaje);
        if (referencia == null) {
            referencia = new Rectangle(origen.x + 40, origen.y + 50, 64, 64);
            panel.setReferencia(referencia);
        }
        if (horizontal) {
            boolean izqToDer = origen.x < destino.x;

            if (izqToDer) {
                while (referencia.x < destino.x - (destino.width / 3)) {
                    referencia.setLocation(referencia.x + 5, referencia.y);
                    retardoTiempo();
                }
            } else {
                while (referencia.x > destino.x + (destino.width / 2)) {
                    referencia.setLocation(referencia.x - 5, referencia.y);
                    retardoTiempo();
                }
            }

        } else {
            boolean arrToBaj = origen.y < destino.y;

            if (arrToBaj) {
                while (referencia.y < destino.y) {
                    referencia.setLocation(referencia.x, referencia.y + 5);
                    retardoTiempo();
                }
            } else {
                while (referencia.y > destino.y + (destino.height / 2)) {
                    referencia.setLocation(referencia.x, referencia.y - 5);
                    retardoTiempo();
                }
            }

        }

    }

    private void retardoTiempo() {
        try {
            this.panel.repaint();
            Thread.sleep(20);
        } catch (InterruptedException ex) {
            Logger.getLogger(hiloAnimacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void animarPushConCrearPaginaImp0() {

        animar(computador, hdd, 1, false, "Almacenando Archivo en Disco");
        animar(hdd, computador, 2, false, "Devolviendo la referencia del archivo almacenado");
        animar(computador, ram, 2, true, "Agregando referencia a la Pila y creando nueva pagina");
        animar(ram, pagina, 2, false, "Almacenando la referencia del archivo en la pagina");
        referencia = new Rectangle(ram.x + 40, ram.y + 50, 64, 64);
        panel.setReferencia(referencia);
        animar(ram, computador, 3, true, "Guardando la pagina en el disco");
        animar(computador, hdd, 3, false, "Guardando la pagina en el disco");

    }

    private void animarPushConBuscarPaginaImp0() {

        animar(computador, hdd, 1, false, "Almacenando Archivo en Disco");
        animar(hdd, computador, 2, false, "Devolviendo la referencia del archivo almacenado");
        animar(computador, ram, 2, true, "Agregando referencia a la pila");
        animar(ram, computador, 2, true, "Obteniendo pagina del disco");
        animar(computador, hdd, 2, false, "Obteniendo pagina del disco");
        animar(hdd, computador, 3, false, "Pagina del disco devuelta");
        animar(computador, ram, 3, true, "Cargando la pagina en memoria RAM");
        animar(ram, pagina, 2, false, "Almacenando la referencia del archivo en la pagina");
        referencia = new Rectangle(ram.x + 40, ram.y + 50, 64, 64);
        panel.setReferencia(referencia);
        animar(ram, computador, 3, true, "Guardando la pagina en el disco");
        animar(computador, hdd, 3, false, "Guardando la pagina en el disco");

    }

    private void animarPopSinEliminarPaginaImp0() {

        animar(computador, ram, 4, true, "Solicitando ultimo archivo ingresado");
        animar(ram, computador, 2, true, "Obteniendo pagina del disco");
        animar(computador, hdd, 2, false, "Obteniendo pagina del disco");
        animar(hdd, computador, 3, false, "Pagina del disco devuelta");
        animar(computador, ram, 3, true, "Cargando la pagina en memoria RAM");
        animar(ram, pagina, 4, false, "Solicitando ultimo archivo ingresado");
        animar(pagina, ram, 2, false, "Devolviendo la referencia del archivo");
        animar(ram, computador, 2, true, "Obteniendo el archivo");
        animar(computador, hdd, 2, false, "Obteniendo el archivo");
        animar(hdd, computador, 1, false, "Devolviendo el archivo al usuario");
        hacerEspera();
        animar(computador, hdd, 5, false, "Eliminando el archivo");
    }

    private void animarPopConEliminarPaginaImp0() {

        animarPopSinEliminarPaginaImp0();
        hacerEspera();
        referencia = new Rectangle(ram.x + 40, ram.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(ram, computador, 5, true, "Eliminando la pagina vacia");
        animar(computador, hdd, 5, false, "Eliminando la pagina vacia");

    }

    private void hacerEspera() {
        try {
            Thread.sleep(800);
        } catch (InterruptedException ex) {
            Logger.getLogger(hiloAnimacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void animarPushSinCrearPaginaImp1() {
        animar(computador, hdd, 1, false, "Almacenando Archivo en Disco");
        animar(hdd, computador, 2, false, "Devolviendo la referencia del archivo almacenado");
        animar(computador, ram, 2, true, "Agregando referencia a la Pila");
        animar(ram, pagina, 2, false, "Almacenando la referencia del archivo en la RAM");
    }

    private void animarPushConCrearPaginaImp1() {
        animar(computador, hdd, 1, false, "Almacenando Archivo en Disco");
        animar(hdd, computador, 2, false, "Devolviendo la referencia del archivo almacenado");
        animar(computador, ram, 2, true, "Agregando referencia a la Pila");
        animar(ram, computador, 3, true, "Almacenando la pagina actuale en RAM");
        animar(computador, hdd, 3, false, "Almacenando la pagina en el disco");
        referencia = new Rectangle(ram.x - 40, ram.y + 40, 64, 64);
        panel.setReferencia(referencia);
        hacerEspera();
        animar(ram, pagina, 2, false, "Almacenando la referencia del archivo en la RAM");
    }

    private void animarPopSinBuscarPaginaImp1() {
        animar(computador, ram, 4, true, "Solicitando ultimo archivo ingresado");
        animar(ram, pagina, 4, false, "Solicitando ultimo archivo ingresado");
        animar(pagina, ram, 2, false, "Devolviendo la referencia del archivo");
        animar(ram, computador, 2, true, "Obteniendo el archivo");
        animar(computador, hdd, 2, false, "Obteniendo el archivo");
        animar(hdd, computador, 1, false, "Devolviendo el archivo al usuario");
        hacerEspera();
        animar(computador, hdd, 5, false, "Eliminando el archivo");
    }

    private void animarPopConBuscarPaginaImp1() {
        animar(computador, ram, 4, true, "Solicitando ultimo archivo ingresado");
        animar(ram, computador, 2, true, "Obteniendo pagina del disco");
        animar(computador, hdd, 2, false, "Obteniendo pagina del disco");
        animar(hdd, computador, 3, false, "Devolviendo la pagina solicitada");
        animar(computador, ram, 3, true, "Cargando la pagina en memoria RAM");
        hacerEspera();
        referencia = new Rectangle(computador.x + 40, computador.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(computador, hdd, 5, false, "Eliminando la pagina devuelta");
        hacerEspera();
        referencia = new Rectangle(ram.x - 40, ram.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(ram, pagina, 4, false, "Solicitando ultimo archivo ingresado");
        animar(pagina, ram, 2, false, "Devolviendo la referencia del archivo");
        animar(ram, computador, 2, true, "Obteniendo el archivo");
        animar(computador, hdd, 2, false, "Obteniendo el archivo");
        animar(hdd, computador, 1, false, "Devolviendo el archivo al usuario");
        hacerEspera();
        animar(computador, hdd, 5, false, "Eliminando el archivo");
    }

    private void animarPushConBuscarPaginaImp1Manual() {
        animar(computador, hdd, 1, false, "Almacenando Archivo en Disco");
        animar(hdd, computador, 2, false, "Devolviendo la referencia del archivo almacenado");
        animar(computador, ram, 2, true, "Agregando referencia a la Pila");
        animar(ram, computador, 3, true, "Obteniendo la pagina solicitada");
        animar(computador, hdd, 3, false, "Obteniendo la pagina solicitada");
        animar(hdd, computador, 3, false, "Pagina del disco devuelta");
        animar(computador, ram, 3, true, "Cargando la pagina en memoria RAM");
        hacerEspera();
        referencia = new Rectangle(computador.x + 40, computador.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(computador, hdd, 5, false, "Eliminando la pagina devuelta");
        hacerEspera();
        referencia = new Rectangle(ram.x - 40, ram.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(ram, pagina, 2, false, "Almacenando la referencia del archivo en la RAM");
    }

    private void animarPushConGuardarPGActaulYBuscarOtraPaginaImp1Manual() {
        animar(computador, hdd, 1, false, "Almacenando Archivo en Disco");
        animar(hdd, computador, 2, false, "Devolviendo la referencia del archivo almacenado");
        animar(computador, ram, 2, true, "Agregando referencia a la Pila");
        animar(ram, computador, 3, true, "Guardando la pagina residente en RAM");
        animar(computador, hdd, 3, false, "Guardando la pagina residente de la RAM en disco");
        animar(hdd, computador, 2, false, "Referencia de la pagina");
        animar(computador, ram, 2, true, "Referencia de la pagina");
        animar(ram, computador, 2, true, "Obteniendo la pagina solicitada");
        animar(computador, hdd, 3, false, "Obteniendo la pagina solicitada");
        animar(hdd, computador, 3, false, "Pagina del disco devuelta");
        animar(computador, ram, 3, true, "Cargando la pagina en memoria RAM");
        hacerEspera();
        referencia = new Rectangle(computador.x + 40, computador.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(computador, hdd, 5, false, "Eliminando la pagina devuelta");
        hacerEspera();
        referencia = new Rectangle(ram.x - 40, ram.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(ram, pagina, 2, false, "Almacenando la referencia del archivo en la RAM");
        referencia = new Rectangle(ram.x + 40, ram.y + 50, 64, 64);
        panel.setReferencia(referencia);
    }

    private void animarPopConGuardarPGActualYBuscarOtraPaginaImp1Manual() {
        animar(computador, ram, 4, true, "Solicitando ultimo archivo ingresado");
        animar(ram, computador, 3, true, "Guardando la pagina residente en RAM");
        animar(computador, hdd, 3, false, "Guardando la pagina residente de la RAM en disco");
        animar(hdd, computador, 2, false, "Referencia de la pagina");
        animar(computador, ram, 2, true, "Referencia de la pagina");
        animar(ram, computador, 2, true, "Obteniendo pagina del disco");
        animar(computador, hdd, 2, false, "Obteniendo pagina del disco");
        animar(hdd, computador, 3, false, "Devolviendo la pagina solicitada");
        animar(computador, ram, 3, true, "Cargando la pagina en memoria RAM");
        hacerEspera();
        referencia = new Rectangle(computador.x + 40, computador.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(computador, hdd, 5, false, "Eliminando la pagina devuelta");
        hacerEspera();
        referencia = new Rectangle(ram.x - 40, ram.y + 40, 64, 64);
        panel.setReferencia(referencia);
        animar(ram, pagina, 4, false, "Solicitando ultimo archivo ingresado");
        animar(pagina, ram, 2, false, "Devolviendo la referencia del archivo");
        animar(ram, computador, 2, true, "Obteniendo el archivo");
        animar(computador, hdd, 2, false, "Obteniendo el archivo");
        animar(hdd, computador, 1, false, "Devolviendo el archivo al usuario");
        hacerEspera();
        animar(computador, hdd, 5, false, "Eliminando el archivo");
    }

}
