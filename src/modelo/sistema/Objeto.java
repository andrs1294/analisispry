/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sistema;

import java.io.File;

/**
 *
 * @author andrs1294
 */
public class Objeto implements Comparable<Object> {

    private Object obj;

    public Objeto(Object hol) {
        this.obj = hol;
    }

    public Object getObjeto() {
        return obj;
    }

    public void setObjeto(Object hol) {
        this.obj = hol;
    }

    @Override
    public String toString() {
        if (obj instanceof Integer) {
            int a = (Integer) obj;
            return "" + a;
        } else if (obj instanceof File) {
            File a = (File) obj;
            return "Archivo: " + a.getName();
        } else if (obj instanceof String) {
            
            return obj.toString();
        }else if(obj instanceof Pagina)
        {
            return "Pagina";
        }
        else 
        {
            return "otro";
        }
    }

    @Override
    public int hashCode() {
       if(this.getObjeto() instanceof Integer)
       {
           return (int)(this.getObjeto());
       }
       return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this.getObjeto() instanceof Integer) {
            int hash = ((Objeto) (obj)).hashCode();
            return hash == (int) (this.getObjeto());
        }
        return super.equals(obj); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int compareTo(Object o) {
        Objeto os = (Objeto) (o);    
        return this.hashCode()-os.hashCode();
    }

}
