/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sistema;

import Excepciones.discoLleno;
import arbol.ElementoExisteException;
import arbol.ElementoNoExisteException;
import java.io.File;
import arbol.arbolB.ArbolB;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andrs1294
 */
public class Disco {

    private ArbolB<Objeto> arbol;
    private int ctdDisco;
    private int bytesOcupados = 0;
    private int accesosaDisco = 0;

    /**
     * Contructor del objeto Disco
     * @param ctdDisco Determina el tamaño en bytes que va a tener el disco
     */
    Disco(int ctdDisco) {
        arbol = new ArbolB(4);
        this.ctdDisco = ctdDisco;

    }


    /**
     * name:agregarObjeto
     * purpose:Agrega un objeto al disco o lo actualiza si ya se encuentra alli.
     * inputs: Objeto que va a ser alamacenado en el disco
     * outputs:Referencia con la cual se puede volver a buscar el mismo archivo
     * complexity: O(1)
     * @param cosa
     * @return La direccion hexadecimal
     */
    int agregarObjeto(Objeto cosa) throws discoLleno {
        int hex = cosa.hashCode();
        int peso = obtenerPesoDelObjeto(cosa);
        if (pesoNoExcedeEspacioLibre(peso)) {
            try {
                arbol.insertar(cosa);
            } catch (ElementoExisteException ex) {
                try {
                    arbol.eliminar(new Objeto(new Integer(hex)));
                    arbol.insertar(cosa);
                    if (cosa.getObjeto() instanceof Pagina) {
                        peso = 0;//lo pongo en 0 puesto que el objeto ya estaba abregado, no es necesario contar su peso de nuevo
                    }else
                    {
                        System.out.println("Se ha modificado un objeto, el peso de este archivo se ha duplciado, resolver esto");
                    }
                } catch (ElementoNoExisteException ex1) {
                    Logger.getLogger(Disco.class.getName()).log(Level.SEVERE, null, ex1);
                } catch (ElementoExisteException ex1) {
                    Logger.getLogger(Disco.class.getName()).log(Level.SEVERE, null, ex1);
                }
            }
            bytesOcupados += peso;
        } else {
            throw new discoLleno("Espacio libre del disco es insuficiente para almacenar este dato");
        }
        accesosaDisco++;
        return hex;
    }

    /**
     * name:pesoNoExcedeEspacioLibre
     * purpose:Verifica que se pueda ingresar un archivo al disco y que no se exceda del espacio libre
     * inputs: Entero que indica el peso del archivo a ingresar
     * outputs:Un boolean que indica Si es posible ingresar ese archivo sin que exceda el disco ocupado
     * complexity: O(1)
     * @param peso Peso del archivo a ingresar
     * @return Si es posible ingresar ese archivo sin que exceda el disco ocupado
     */
    private boolean pesoNoExcedeEspacioLibre(int peso) {
        return peso + bytesOcupados <= ctdDisco;
    }

    /**
     * name:obtenerObjeto
     * purpose:Obtiene un objeto del disco dada su referencia
     * inputs: Entero que indica la referencia del objeto
     * outputs:El objeto almacenado en disco
     * complexity: O(1)
     * @param Hex Referencia Hex del archivo a buscar
     * @return EL objeto guardado en el disco con la referencia dada por parametro
     */
    Objeto obtenerObjeto(int Hex) {
        accesosaDisco++;
        return arbol.buscar(new Objeto(new Integer(Hex)));
    }

    /**
     * name:obtenerPesoDelObjeto
     * purpose:retorna el numero de Bytes que ocupa el objeto a guardar
     * inputs:El objeto del cual se desea saber su peso
     * outputs:Numero de Bytes que pesa el objeto
     * complexity: O(1)
     * @param cosa
     * @return Numero de Bytes
     */
    private int obtenerPesoDelObjeto(Objeto cos) {
        Object cosa = cos.getObjeto();
        int peso = 0;
        if (cosa instanceof String) {
            peso = ((String) (cosa)).length();
        } else if (cosa instanceof File) {
            peso = (int) ((File) (cosa)).length();
        } else if (cosa instanceof Pagina) {
            peso = ((Pagina) (cosa)).lengthEnBytes();
        }

        return peso;
    }

    /**
     * name:eliminarObjeto
     * purpose:Dado un objeto, lo elimina del disco
     * inputs:El objeto el cual se desea eliminar
     * outputs:
     * complexity: O(1)
     * @param paginaObj Objeto a ser eliminado
     */
    void eliminarObjeto(Objeto paginaObj) {
        try {
            accesosaDisco++;
            arbol.eliminar(paginaObj);
            bytesOcupados -= obtenerPesoDelObjeto(paginaObj);
        } catch (ElementoNoExisteException ex) {
            Logger.getLogger(Disco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * name:reiniciarAccesosDisco
     * purpose:Vuelve a Cero el número de accesos a Disco 
     * inputs:
     * outputs:
     * complexity: O(1)
     */
    public void reiniciarAccesosDisco() {
        accesosaDisco = 0;
    }

    /**
     * @return El numero de accesos a Disco
     */
    public int getAccesosDisco() {
        return accesosaDisco;
    }

    /**
     * name:getPorcentajeOcupado
     * purpose:Indica el porcentaje del disco ocupado
     * inputs:
     * outputs:Un numero entre 1 a 100 de la cantidad del disco ocupado
     * complexity: O(1)
     * @return Un numero entre 1 a 100 de la cantidad del disco ocupado
     */
    public float getPorcentajeOcupado() {
        float porc = (bytesOcupados * 100f) / ctdDisco;

        return porc;
    }

    /**
     * name:eliminarPagina
     * purpose:Elimina una pagina dada su referencia 
     * inputs:El numero de referencia de esa pagina
     * outputs:Un numero entre 1 a 100 de la cantidad del disco ocupado
     * complexity: O(1)
     * @param paginaHex El numero de referencia de esa pagina
     */
    void eliminarPagina(int paginaHex) {
        try {
            arbol.eliminar(new Objeto(new Integer(paginaHex)));
            accesosaDisco++;
        } catch (ElementoNoExisteException ex) {
            System.out.println("No existe el objeto " + ex.getMessage() );
        }
    }

    void eliminarAccesoDisco() {
        accesosaDisco--;
    }

    public ArbolB getArbol() {
        return arbol;
    }
}
