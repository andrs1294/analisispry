/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sistema;

import Excepciones.discoLleno;
import Excepciones.paginaLlenaExcepcion;
import Excepciones.ramLlenaExcepcion;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andrs1294
 */
public class Sistema {

    private Ram ram;
    private Disco disco;
    private Pila pila;
    private int implementacion;
    public static boolean automatico;
    public Sistema(int ctdPalabras, int ctdRam, int ctdDisco, int implementacion,boolean automatico,int cantidadPag) throws ramLlenaExcepcion {
        ram = new Ram(ctdRam, this, implementacion);
        ram.setCantidadPalabrasPaginas(ctdPalabras);
        disco = new Disco(ctdDisco);
        pila = new Pila(getRam());
        this.implementacion=implementacion;
        Sistema.automatico=automatico;
        if(!Sistema.automatico)
        {
            ram.crearPaginasManual(cantidadPag);
        }

    }

    /**
     * name:guardarObjetoEnDisco
     * purpose:Guarda el objeto en disco
     * inputs:El objeto a ser guardado
     * outputs:La referencia del objeto guardado en disco
     * complexity: O(1)
     */
     int guardarObjetoEnDisco(Objeto obj) throws discoLleno
    {
        return getDisco().agregarObjeto(obj);
    }
     
     /**
      * name:push
     * purpose:Almacena el objeto dentro de la pagina dada
     * inputs:El objeto a ser guardado y la pagina donde se desea almacenar dicho archivo, -1 para indicar en automatico
     * outputs:
     * complexity: O(n)
      */
    public void push(Object cosa,int pag) throws discoLleno, ramLlenaExcepcion, paginaLlenaExcepcion {
        int hexObj=0;
        try {
             hexObj = guardarObjetoEnDisco(new Objeto(cosa)); //Agrega el objeto al disco, y me retorna un valor hexadecimal que me indica la posicion de ese archivo en el disco
            Objeto pagina = getPila().push(hexObj,pag); //agrega el objeto a la pila
            if (pagina != null) {
                if(!paginaCompletamenteLlena(pagina))//esto se hace para evitar sobreescribir la pagina si viene de la implementacion 1
                {
                    pagina.setObjeto(getRam().obtenerPaginaResidente());
                }
                
                guardarObjetoEnDisco(pagina); //escribo de nuevo la pagina en disco
            }
        } catch (paginaLlenaExcepcion ex) {
            //Como ya se ingreso el archivo al disco pero no se pudo ingresar en la pagina porque esta llena entonces debo eliminarlo
            eliminarPaginaDelDisco(hexObj);
            throw new paginaLlenaExcepcion("");
        }

    }


    public Objeto obtenerObjeto(int paginaHex) {
        return getDisco().obtenerObjeto(paginaHex);
    }

    public float getDiscoOcupado() {
        return disco.getPorcentajeOcupado();
    }

    public float getRamOcupada(int operacion) {
        return ram.porcentajeOcupado(operacion);
    }

    /**
     * name:pop
     * purpose:Almacena el objeto dentro de la pagina dada
     * inputs:La pagina de la cual se desea obtener el TOP
     * outputs:Un arreglo de dos posiciones donde la primera es la pagina actual, y 
     * la segunda es el archivo el cual se saco de la pila
     * complexity: O(n)
     * @return Un arreglo de dos posiciones donde la primera es la pagina actual, y 
     * la segunda es el archivo el cual se saco de la pila
     */
    public Objeto[] pop(int pag) {
        Objeto obj[] = pila.pop(this, pag); 
        if (obj != null) {
            
            eliminarObjeto(obj[1]);
        }

        return obj;
    }

    /**
     * @return the ram
     */
    public Ram getRam() {
        return ram;
    }

    void eliminarObjeto(Objeto paginaObj) {
        disco.eliminarObjeto(paginaObj);
    }

    /**
     * @param ram the ram to set
     */
    public void setRam(Ram ram) {
        this.ram = ram;
    }

    /**
     * @return the disco
     */
    public Disco getDisco() {
        return disco;
    }

    /**
     * @param disco the disco to set
     */
    public void setDisco(Disco disco) {
        this.disco = disco;
    }

    /**
     * @return the pila
     */
    public Pila getPila() {
        return pila;
    }

    /**
     * @param pila the pila to set
     */
    public void setPila(Pila pila) {
        this.pila = pila;
    }

    public void reiniciarAccesosDisco() {
        disco.reiniciarAccesosDisco();
    }

    public int getAccesosDisco() {
        return disco.getAccesosDisco();
    }

    /**
     * name:paginaCompletamenteLlena
     * purpose:Indica si una pagina se encuentre totalmente llena
     * inputs:La pagina de la cual se desea saber si esta llena
     * outputs:Verdadero si la pagina se encuentra llena
     * complexity: O(n) donde n es la candidad de palabras por pagina
     */
    private boolean paginaCompletamenteLlena(Objeto obj) {
       
        for (int i : ((Pagina)(obj.getObjeto())).getPagina()) {
            if(i==0)
                return false;
        }
        return true;
    }

    void eliminarPaginaDelDisco(int paginaHex) {
       disco.eliminarPagina(paginaHex);
    }

    /**
     * @return the implementacion
     */
    public int getImplementacion() {
        return implementacion;
    }

    /**
     * Retorna el rango de las paginas actuales en la memoria ram
     * @return String con el rango de las paginas 
     */
    public int getPaginaFinal() {
       return ram.getRangoPaginas();
    }
}
