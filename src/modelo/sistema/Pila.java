/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo.sistema;

import Excepciones.paginaLlenaExcepcion;
import Excepciones.ramLlenaExcepcion;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author andrs1294
 */
public class Pila {

    private Ram ram;

    Pila(Ram ram) {
        this.ram = ram;
        establecarTopEnNegativo();
    }

    /**
     * name:push
     * purpose:Agrega la referencia de un objeto a un numero de pagina 
     * inputs:HexObj indica la referencia del objeto y pag indica el numero de la pagina donde se va a ingresar(no la referencia)
     * outputs:Un objeto que es la pagina donde se almaceó el archivo
     * complexity: O(n) 
     * @param hexObj intero que hace referencia al objeto guardado en el disco
     */
    Objeto push(int hexObj, int pag) throws ramLlenaExcepcion, paginaLlenaExcepcion {
        try {
            incrementarTop(pag);
            int pagina = obtenerPaginaTop(pag);
            int posicion = obtenerPosElementoTop(pag);
            boolean posible = verificarNoSobrepasarLimite(pagina);
            if (posible) {
                Objeto pg = ram.colocarPagina(pagina);
                Objeto pg2 = ram.agregarEntrada(hexObj, posicion, pagina); //Este metodo me devuelve la pagina por si al agregar un elemento la pagina ya esta llena
                return (pg != null) ? pg : pg2;
            } else {
                decrementarTop(pag);
                throw new ramLlenaExcepcion("Error, no se pueden ingresar mas datos, memoria llena");
            }
        } catch (paginaLlenaExcepcion ex) {
            decrementarTop(pag);
            throw new paginaLlenaExcepcion("");
        }

    }

    /**
     * Me retorna el numero de la pagina donde se debe almacenar la direccion
     * del archivo
     * name:obtenerPaginaTop
     * purpose:Obtiene la pagina donde se encuentra actualmente el Top de la pila
     * inputs:Si es la implementacion donde se decide a que pagina se realiza la operacion se indica el numero
     * de lo contrario con -1 se indica que el sistema es automatico
     * outputs:Un objeto que es la pagina donde se almaceó el archivo
     * complexity: O(n) 
     * @return
     */
    private int obtenerPaginaTop(int pag) {
        if (pag == -1) {
            return getTop() / ram.getCantidadPalabrasPaginas();
        } else {
            return pag;
        }
    }

    int getTop() {
        return ram.getTop();
    }

    void incrementarTop(int pag) {
        if (pag == -1) {
            ram.incrementarTop();
        } else {
            ram.incrementarPointer(pag);
        }
    }

    /**
     * name:obtenerPosElementoTop
     * purpose:Obtiene la posicion donde se ha de almacenar la referencia dentro de la pagina
     * inputs:Si es la implementacion donde se decide a que pagina se realiza la operacion se indica el numero de la pagina
     * de lo contrario con -1 se indica que el sistema es automatico
     * outputs:Retorna la posicion dentro de la pagina donde se debe almacenar ese archivo
     * complexity: O(1)
     * @return
     */
    public int obtenerPosElementoTop(int pag) throws paginaLlenaExcepcion {
        if (pag == -1) {
            return getTop() % Ram.getCantidadPalabrasPaginas();
        } else {
            int pos = ram.getPosElemento(pag);
            if (pos >= Ram.cantidadPalabrasPaginas) {
                throw new paginaLlenaExcepcion("La pagina actual esta llena");
            }
            return pos;
        }
    }

    /**
     * name:pop
     * purpose:Obtiene el ultimo elemento ingresado a la pila o a la pagina
     * inputs:system Es la referencia al sistema que me permite eliminar una
     * pagina del disco si esta ha quedado totalmente vacia y la pagina de la cual se desea 
     * obtener el ultimo elemento ingresado, -1 para indicar que es automatico
     * outputs:Un arreglo de 2 posiciones, donde la primera es la pagina
     * residente en memoria, y la segunda es el objeto top que va a ser retirado
     * complexity: O(1) 
     * @param system Es la referencia al sistema que me permite eliminar una
     * pagina del disco si esta ha quedado totalmente vacia
     * @return Un arreglo de 2 posiciones, donde la primera es la pagina
     * residente en memoria, y la segunda es el objeto top que va a ser retirado
     */
    Objeto[] pop(Sistema system, int pagi) {
        try {
            int pagina = obtenerPaginaTop(pagi);
            int posicion = obtenerPosElementoTop(pagi);
            if (posicion == -1) {
                return null; //retorno null puesto que la pila esta vacia
            }
            Objeto paginaObj = ram.colocarPagina(pagina);
            Objeto cosa = null;
            if (ram.getImplementacion() == 0) {
                cosa = ram.eliminarTop(posicion, paginaObj);
                if (cosa != null) {
                    Pagina pg = (Pagina) (paginaObj.getObjeto());
                    if (pg.isVacia()) {
                        if (Sistema.automatico) {
                            system.eliminarObjeto(paginaObj);
                            ram.borrarPagina(pagina);
                        }

                    }
                    //poner referencia de esa posicion en 0
                    decrementarTop(pagi);
                }
            } else if (ram.getImplementacion() == 1) {
                cosa = ram.eliminarTopImp2(posicion, pagina);
                if (cosa != null) {
                    decrementarTop(pagi);
                }
            }

            return new Objeto[]{new Objeto(ram.obtenerPaginaResidente()), cosa};
        } catch (paginaLlenaExcepcion ex) {
            Logger.getLogger(Pila.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private void decrementarTop(int pag) {
        ram.decremetarTop(pag);
    }

    /**
     * name:verificarNoSobrepasarLimite
     * purpose:Verifica que no se creen mas paginas de las que es posible almancenar en memoria RAM
     * inputs:Entero que me indica el conteo del total de paginas 
     * outputs:Un boolean que me indica si se puede crear esa pagina
     * complexity: O(1)
     * @param pagina
     * @return 
     */
    private boolean verificarNoSobrepasarLimite(int pagina) {
        if (pagina <= ram.ram.length - Ram.cantidadPalabrasPaginas - 2) {
            return true;
        }

        return false;
    }

    private void establecarTopEnNegativo() {
            ram.decremetarTop(-1);
    }

}
